package com.example.pk.test.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pk.test.R;
import com.example.pk.test.models.CurrencyModel;

import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SimpleCurrencyInformationRVAdapter extends RecyclerView.Adapter<SimpleCurrencyInformationRVAdapter.ViewHolder>{
    public interface OnItemClickListener {
        void onItemClick(CurrencyModel item, int position);
    }

    private Context context;
    private ArrayList<CurrencyModel> currencyModels;
    private OnItemClickListener onItemClickListener;
    private AssetManager mAssetManager;


    public SimpleCurrencyInformationRVAdapter(Context context, ArrayList<CurrencyModel> currencyModels) {
        this.context = context;
        this.currencyModels = currencyModels;
        mAssetManager = context.getAssets();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View root = layoutInflater.inflate(R.layout.sci_rv_item, parent, false);

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.currencyCode.setText(currencyModels.get(position).base);
        holder.currencySimpleInfo.setText(currencyModels.get(position).simpleInfo);
        try {
            InputStream inputStream = mAssetManager.open(currencyModels.get(position).base + ".png");
            holder.currencyIcon.setImageDrawable(Drawable.createFromStream(inputStream, null));
        } catch (Exception e) {}
    }

    @Override
    public int getItemCount() {
        return currencyModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.sci_rv_item_currency_code)
        TextView currencyCode;
        @BindView(R.id.sci_rv_item_currency_simple_information)
        TextView currencySimpleInfo;
        @BindView(R.id.sci_currency_icon)
        ImageView currencyIcon;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener
                        .onItemClick(currencyModels.get(getAdapterPosition()), getAdapterPosition());
            } else {
                throw new RuntimeException("You must init OnItemClickListener " +
                        "before call method onItemClick()");
            }
        }
    }
}

package com.example.pk.test.UI;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pk.test.R;
import com.example.pk.test.adapters.CurrencyRateRVAdapter;
import com.example.pk.test.models.CurrencyModel;
import com.google.gson.Gson;

import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyRateActivity extends AppCompatActivity {
    @BindView(R.id.acr_currency_icon)
    ImageView currencyIcon;
    @BindView(R.id.acr_currency_code)
    TextView currencyCode;
    @BindView(R.id.acr_currency_info)
    TextView currencySimpleInfo;
    @BindView(R.id.acr_currency_rate_rv)
    RecyclerView currencyRates;

    private CurrencyModel currencyModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_rate);
        ButterKnife.bind(this);

        String extra = getIntent().getStringExtra(SimpleInfoCurrencyActivity.CURRENCY_MODEL_KEY);

        currencyModel = new Gson().fromJson(extra, CurrencyModel.class);

        currencyRates.setLayoutManager(new LinearLayoutManager(this));

        currencyRates.setAdapter(new CurrencyRateRVAdapter(this, currencyModel.currencyRates));
        setCurrencyInformation();

        try {
            InputStream inputStream = getAssets().open(currencyModel.base + ".png");
            currencyIcon.setImageDrawable(Drawable.createFromStream(inputStream, null));
        } catch (Exception e) {}
    }

    public void setCurrencyInformation() {
        currencyCode.setText(currencyModel.base);
        currencySimpleInfo.setText(currencyModel.info);
    }
}

package com.example.pk.test.net;

import com.example.pk.test.models.QueryCurrencyRateModel;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

public class Retrofit {
    private static String CURRENCY_RATE_BASE_URL = "http://api.fixer.io";

    private static CurrencyRateAPI currencyRateAPI;

    static {
        initialize();
    }

    private interface CurrencyRateAPI {
        @GET("/latest")
        void getCurrencyRate(@Query("base") String baseCurrency
                , @Query("symbols") String currencies, Callback<QueryCurrencyRateModel> callback);
    }

    private static void initialize() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(CURRENCY_RATE_BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        currencyRateAPI = adapter.create(CurrencyRateAPI.class);
    }


    public static void getCurrencyRate(String baseCurrency, String currencies
            , Callback<QueryCurrencyRateModel> callback) {
        currencyRateAPI.getCurrencyRate(baseCurrency, currencies, callback);
    }
}

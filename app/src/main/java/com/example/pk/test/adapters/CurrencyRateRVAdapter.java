package com.example.pk.test.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pk.test.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyRateRVAdapter extends RecyclerView.Adapter<CurrencyRateRVAdapter.ViewHolder> {
    private Context context;
    private HashMap<String, Double> currencyRates;
    private ArrayList<String> currencyCodes;
    private ArrayList<Double> currencyValue;
    private AssetManager mAssetManager;

    public CurrencyRateRVAdapter(Context context, HashMap<String, Double> currencyRates) {
        this.context = context;
        this.currencyRates = currencyRates;
        mAssetManager = context.getAssets();

        currencyCodes = new ArrayList<>();
        currencyValue = new ArrayList<>();

        for (String key : currencyRates.keySet()) {
            currencyCodes.add(key);
            currencyValue.add(currencyRates.get(key));
        }

    }

    @Override
    public CurrencyRateRVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View root = layoutInflater.inflate(R.layout.cr_rv_item, parent, false);

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(CurrencyRateRVAdapter.ViewHolder holder, int position) {
        holder.currencyCode.setText(currencyCodes.get(position));
        holder.currencyRate.setText(String.valueOf(currencyValue.get(position)));
        try {
            InputStream inputStream = mAssetManager.open(currencyCodes.get(position) + ".png");
            holder.currencyIcon.setImageDrawable(Drawable.createFromStream(inputStream, null));
        } catch (Exception e) {
        }

        switch (currencyCodes.get(position)) {
            case "AUD":
                holder.currencySimpleInfo.setText(context.getString(R.string.AUDsimpleInfo));
                break;
            case "BRL":
                holder.currencySimpleInfo.setText(context.getString(R.string.BRLsimpleInfo));
                break;
            case "CNY":
                holder.currencySimpleInfo.setText(context.getString(R.string.CNYsimpleInfo));
                break;
            case "EUR":
                holder.currencySimpleInfo.setText(context.getString(R.string.EURsimpleInfo));
                break;
            case "SGD":
                holder.currencySimpleInfo.setText(context.getString(R.string.SGDsimpleInfo));
                break;
            case "JPY":
                holder.currencySimpleInfo.setText(context.getString(R.string.JPYsimpleInfo));
                break;
            case "MXN":
                holder.currencySimpleInfo.setText(context.getString(R.string.MXNsimpleInfo));
                break;
            case "MYR":
                holder.currencySimpleInfo.setText(context.getString(R.string.MYRsimpleInfo));
                break;
            case "NZD":
                holder.currencySimpleInfo.setText(context.getString(R.string.NZDsimpleInfo));
                break;
            case "USD":
                holder.currencySimpleInfo.setText(context.getString(R.string.USDsimpleInfo));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return currencyCodes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cr_rv_item_currency_code)
        TextView currencyCode;
        @BindView(R.id.cr_rv_item_currency_simple_information)
        TextView currencySimpleInfo;
        @BindView(R.id.cr_rv_item_currency_rate)
        TextView currencyRate;
        @BindView(R.id.cr_currency_icon)
        ImageView currencyIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}

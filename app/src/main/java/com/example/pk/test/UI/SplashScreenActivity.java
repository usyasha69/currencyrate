package com.example.pk.test.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.pk.test.R;
import com.example.pk.test.models.CurrencyModel;
import com.example.pk.test.models.QueryCurrencyRateModel;
import com.example.pk.test.net.Retrofit;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashScreenActivity extends AppCompatActivity {
    public static final String CURRENCY_MODELS_KEY = "currency models";

    private ArrayList<CurrencyModel> currencyModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //get base currencies codes
        Retrofit.getCurrencyRate("PHP", "AUD,BRL,CNY,EUR,SGD,JPY,MXN,MYR,NZD,USD",
                new Callback<QueryCurrencyRateModel>() {
                    @Override
                    public void success(QueryCurrencyRateModel queryCurrencyRateModel, Response response) {
                        createAndFillingCurrencyRateModels(queryCurrencyRateModel);

                        startActivity(new Intent(SplashScreenActivity.this
                                , SimpleInfoCurrencyActivity.class).putExtra(CURRENCY_MODELS_KEY
                                , new Gson().toJson(currencyModels)));
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void createAndFillingCurrencyRateModels(QueryCurrencyRateModel queryCurrencyRateModel) {
        currencyModels = new ArrayList<>();

        //create currency models and filling fields: base, info, simple info.
        for (String currencyCode : queryCurrencyRateModel.rates.keySet()) {

            CurrencyModel currencyModel = new CurrencyModel();
            currencyModel.base = currencyCode;

            switch (currencyCode) {
                case "AUD":
                    currencyModel.simpleInfo = getString(R.string.AUDsimpleInfo);
                    currencyModel.info = getString(R.string.AUDinfo);
                    break;
                case "BRL":
                    currencyModel.simpleInfo = getString(R.string.BRLsimpleInfo);
                    currencyModel.info = getString(R.string.BRLInfo);
                    break;
                case "CNY":
                    currencyModel.simpleInfo = getString(R.string.CNYsimpleInfo);
                    currencyModel.info = getString(R.string.CNYInfo);
                    break;
                case "EUR":
                    currencyModel.simpleInfo = getString(R.string.EURsimpleInfo);
                    currencyModel.info = getString(R.string.EURInfo);
                    break;
                case "SGD":
                    currencyModel.simpleInfo = getString(R.string.SGDsimpleInfo);
                    currencyModel.info = getString(R.string.SGDInfo);
                    break;
                case "JPY":
                    currencyModel.simpleInfo = getString(R.string.JPYsimpleInfo);
                    currencyModel.info = getString(R.string.JPYInfo);
                    break;
                case "MXN":
                    currencyModel.simpleInfo = getString(R.string.MXNsimpleInfo);
                    currencyModel.info = getString(R.string.MXNInfo);
                    break;
                case "MYR":
                    currencyModel.simpleInfo = getString(R.string.MYRsimpleInfo);
                    currencyModel.info = getString(R.string.MYRInfo);
                    break;
                case "NZD":
                    currencyModel.simpleInfo = getString(R.string.NZDsimpleInfo);
                    currencyModel.info = getString(R.string.NZDInfo);
                    break;
                case "USD":
                    currencyModel.simpleInfo = getString(R.string.USDsimpleInfo);
                    currencyModel.info = getString(R.string.USDInfo);
                    break;
            }

            currencyModels.add(currencyModel);
        }
    }
}

package com.example.pk.test.models;

import java.util.HashMap;

public class QueryCurrencyRateModel {
    public String base;
    public String date;
    public HashMap<String, Double> rates;
}

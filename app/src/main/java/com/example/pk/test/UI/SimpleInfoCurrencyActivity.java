package com.example.pk.test.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.pk.test.R;
import com.example.pk.test.adapters.SimpleCurrencyInformationRVAdapter;
import com.example.pk.test.models.CurrencyModel;
import com.example.pk.test.models.QueryCurrencyRateModel;
import com.example.pk.test.net.Retrofit;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SimpleInfoCurrencyActivity extends AppCompatActivity {
    public static final String CURRENCY_MODEL_KEY = "currency model";

    @BindView(R.id.sica_simple_currency_info_rv)
    RecyclerView simpleCurrencyInfo;

    private ArrayList<CurrencyModel> currencyModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_info_currency);
        ButterKnife.bind(this);

        String extra = getIntent().getStringExtra(SplashScreenActivity.CURRENCY_MODELS_KEY);
        currencyModels = new Gson().fromJson(extra, new TypeToken<List<CurrencyModel>>(){}.getType());
        simpleCurrencyInfo.setLayoutManager(new LinearLayoutManager(this));

        SimpleCurrencyInformationRVAdapter simpleCurrencyInformationRVAdapter =
                new SimpleCurrencyInformationRVAdapter(this, currencyModels);
        simpleCurrencyInformationRVAdapter.setOnItemClickListener(new SimpleCurrencyInformationRVAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CurrencyModel item, int position) {
                final CurrencyModel fItem = item;

                Retrofit.getCurrencyRate(item.base, getCurrencies(item, currencyModels)
                        , new Callback<QueryCurrencyRateModel>() {
                            @Override
                            public void success(QueryCurrencyRateModel queryCurrencyRateModel, Response response) {
                                fItem.currencyRates = queryCurrencyRateModel.rates;

                                startActivity(new Intent(SimpleInfoCurrencyActivity.this
                                        , CurrencyRateActivity.class).putExtra(CURRENCY_MODEL_KEY, new Gson().toJson(fItem)));
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
            }
        });

        simpleCurrencyInfo.setAdapter(simpleCurrencyInformationRVAdapter);
    }

    private String getCurrencies(CurrencyModel currencyModel, ArrayList<CurrencyModel> currencyModels) {
        String result = "";

        for (int i = 0; i < currencyModels.size(); i++) {
            if (!currencyModels.get(i).base.equals(currencyModel.base)) {
                if (i == currencyModels.size() - 1) {
                    result += currencyModels.get(i).base;
                } else {
                    result += currencyModels.get(i).base + ",";
                }
            }
        }

        return result;
    }
}
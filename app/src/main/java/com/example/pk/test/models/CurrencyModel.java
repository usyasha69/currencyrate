package com.example.pk.test.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class CurrencyModel implements Parcelable {
    public String base;
    public String simpleInfo;
    public String info;
    public HashMap<String, Double> currencyRates;

    public CurrencyModel() {
    }

    protected CurrencyModel(Parcel in) {
        base = in.readString();
        simpleInfo = in.readString();
        info = in.readString();

        if (currencyRates == null) {
            currencyRates = new HashMap<>();

            int size = in.readInt();
            for (int i = 0; i < size; i++) {
                String key = in.readString();
                double value = in.readDouble();

                currencyRates.put(key, value);
            }
        }
    }

    public static final Creator<CurrencyModel> CREATOR = new Creator<CurrencyModel>() {
        @Override
        public CurrencyModel createFromParcel(Parcel in) {
            return new CurrencyModel(in);
        }

        @Override
        public CurrencyModel[] newArray(int size) {
            return new CurrencyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(base);
        parcel.writeString(simpleInfo);
        parcel.writeString(info);

        if (currencyRates != null) {
            parcel.writeInt(currencyRates.size());
            for (Map.Entry<String, Double> entry : currencyRates.entrySet()) {
                parcel.writeString(entry.getKey());
                parcel.writeDouble(entry.getValue());
            }
        }
    }
}
